#!/bin/bash
ip_addr=$(ifconfig ens33 | grep 'inet addr' | cut -d: -f2 | awk '{print $1}')
echo "0" > /proc/sys/net/ipv4/ip_forward
iptables -t nat -D PREROUTING -p tcp --dport 502 -j DNAT --to-destination $ip_addr:502


